import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { loadMovies } from '../../actions';
import Loading from '../../components/Loading/Loading';
import MoviesPage from '../../components/MoviesPage/MoviesPage';

export const Movie = ({ match, dispatch, movies }) => {
  const { loading, data } = movies;
  const isLoading = (!loading && !data) || loading;

  useEffect(() => {
    if (!loading && !data) {
      dispatch(loadMovies());
    }
  });

  if (isLoading) {
    return <Loading />;
  }

  const items = data.filter(({ id }) => id === match.params.id);

  if (items.length <= 0) {
    return <Redirect to="/movies" />;
  }

  return <MoviesPage movie={items[0]} />;
};

Movie.propTypes = {
  match: PropTypes.shape({}).isRequired,
  dispatch: PropTypes.func.isRequired,
  movies: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    data: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          title: PropTypes.string.isRequired,
          storyline: PropTypes.string.isRequired,
          posterurl: PropTypes.string.isRequired,
        })
      ),
    ]),
  }),
};

Movie.defaultProps = {
  movies: {
    loading: false,
    data: false,
  },
};

export default connect(({ movies }) => ({ movies }))(Movie);
