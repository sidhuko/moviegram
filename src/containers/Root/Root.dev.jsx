import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import DevTools from '../DevTools/DevTools';

const Root = ({ store = {}, children }) => (
  <Provider store={store}>
    {children}
    <DevTools />
  </Provider>
);

Root.propTypes = {
  store: PropTypes.shape({}).isRequired,
  children: PropTypes.element.isRequired,
};

export default Root;
