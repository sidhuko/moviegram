import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadMovies, searchMovies } from '../../actions';
import Loading from '../../components/Loading/Loading';
import MoviesList from '../../components/MoviesList/MoviesList';

export const Movies = ({ dispatch, movies, search }) => {
  const isLoading = (!movies.loading && !movies.data) || movies.loading;

  useEffect(() => {
    if (!movies.loading && !movies.data) {
      dispatch(loadMovies());
    }
  });

  if (isLoading) {
    return <Loading />;
  }

  return (
    <MoviesList
      query={search.query || false}
      onSearch={query => dispatch(searchMovies(query))}
      search={search.data || []}
      movies={movies.data || []}
    />
  );
};

Movies.propTypes = {
  dispatch: PropTypes.func.isRequired,
  search: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    data: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          title: PropTypes.string.isRequired,
          storyline: PropTypes.string.isRequired,
          posterurl: PropTypes.string.isRequired,
        })
      ),
    ]),
  }),
  movies: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    data: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.string.isRequired,
          title: PropTypes.string.isRequired,
          storyline: PropTypes.string.isRequired,
          posterurl: PropTypes.string.isRequired,
        })
      ),
    ]),
  }),
};

Movies.defaultProps = {
  search: {
    loading: false,
    data: false,
  },
  movies: {
    loading: false,
    data: false,
  },
};

export default connect(({ movies, search }) => ({ movies, search }))(Movies);
