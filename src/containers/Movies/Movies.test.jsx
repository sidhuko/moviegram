import React from 'react';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { Movies } from './Movies';

describe('Movies', () => {
  describe('when rendered', () => {
    const dispatch = jest.fn();

    afterAll(() => {
      dispatch.mockReset();
    });

    const tree = mount(
      <MemoryRouter>
        <Movies dispatch={dispatch} movies={{ loading: false, data: false }} />
      </MemoryRouter>
    );

    it('should match snapshot for rendered', () => {
      expect(tree.find(Movies)).toMatchSnapshot();
    });

    it('should load movies', () => {
      expect(dispatch).toHaveBeenCalledWith({ type: 'MOVIES_REQUEST' });
    });
  });

  describe('when loading', () => {
    const dispatch = jest.fn();

    const tree = mount(
      <MemoryRouter>
        <Movies dispatch={dispatch} movies={{ loading: true, data: false }} />
      </MemoryRouter>
    );

    afterEach(() => {
      dispatch.mockReset();
    });

    it('should match snapshot for loading', () => {
      expect(tree.find(Movies)).toMatchSnapshot();
    });

    it('should call dispatch', () => {
      expect(dispatch).not.toHaveBeenCalled();
    });
  });

  describe('when loaded', () => {
    const dispatch = jest.fn();

    const title = 'Example Title';
    const storyline = 'Example storyline';
    const posterurl = 'an-image.jpg';

    const movies = [
      {
        id: '1',
        title,
        storyline,
        posterurl,
      },
      {
        id: '2',
        title,
        storyline,
        posterurl,
      },
    ];

    const tree = mount(
      <MemoryRouter>
        <Movies dispatch={dispatch} movies={{ loading: false, data: movies }} />
      </MemoryRouter>
    );

    afterEach(() => {
      dispatch.mockReset();
    });

    it('should match snapshot for loaded', () => {
      expect(tree.find(Movies)).toMatchSnapshot();
    });

    it('should contain two movies', () => {
      expect(tree.find('MoviesListItem')).toHaveLength(2);
    });
  });
});
