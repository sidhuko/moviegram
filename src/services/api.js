import 'isomorphic-fetch';

const BASE_URL = 'http://localhost:3000';

async function request(url) {
  const response = await fetch(url);
  const data = await response.json();

  if (!response.ok) {
    return { error: data.message || 'Something bad happened' };
  }

  return data;
}

export default async () => request(`${BASE_URL}/movies.json`);
