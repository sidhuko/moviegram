import { combineReducers } from 'redux';
import { MOVIES, SEARCH } from '../actions';

export function movies(state = { loading: false }, { type, data }) {
  switch (type) {
    case MOVIES.REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case MOVIES.SUCCESS:
      return Object.assign({}, state, {
        data,
        loading: false,
      });
    case MOVIES.FAILURE:
      return Object.assign({}, state, {
        loading: false,
      });
    default:
      return state;
  }
}

export function search(state = { loading: false }, { type, data, query }) {
  switch (type) {
    case SEARCH.REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case SEARCH.SUCCESS:
      return Object.assign({}, state, {
        data,
        query,
        loading: false,
      });
    case SEARCH.FAILURE:
      return Object.assign({}, state, {
        loading: false,
      });
    default:
      return state;
  }
}

export default combineReducers({
  movies,
  search,
});
