import { search, movies } from './index';
import { MOVIES, SEARCH } from '../actions';

describe('Search reducer', () => {
  const data = ['test'];

  it('when request made should start loading', () => {
    expect(search({}, { type: SEARCH.REQUEST })).toEqual({ loading: true });
  });

  it('when request failed should stop loading', () => {
    expect(search({}, { type: SEARCH.FAILURE })).toEqual({ loading: false });
  });

  it('when request sucess should stop loading and store results', () => {
    expect(search({}, { type: SEARCH.SUCCESS, data })).toEqual({
      loading: false,
      data,
    });
  });
});

describe('Movies reducer', () => {
  const data = ['test'];

  it('when request made should start loading', () => {
    expect(movies({}, { type: MOVIES.REQUEST })).toEqual({ loading: true });
  });

  it('when request failed should stop loading', () => {
    expect(movies({}, { type: MOVIES.FAILURE })).toEqual({ loading: false });
  });

  it('when request sucess should stop loading and store results', () => {
    expect(movies({}, { type: MOVIES.SUCCESS, data })).toEqual({
      loading: false,
      data,
    });
  });
});
