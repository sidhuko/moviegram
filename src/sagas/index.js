import { all, put, call, fork, takeEvery } from 'redux-saga/effects';
import { MOVIES, SEARCH } from '../actions';
import api from '../services/api';

function* sagaMoviesLoad() {
  try {
    const data = yield call(api);
    yield put({ type: MOVIES.SUCCESS, data });
  } catch (error) {
    yield put({ type: MOVIES.FAILURE, error });
  }
}

function* sagaMoviesSearch({ query }) {
  try {
    const data = yield call(api);

    yield put({
      type: SEARCH.SUCCESS,
      query: query || false,
      data: data.filter(item => item.title.includes(query)),
    });
  } catch (error) {
    yield put({ type: SEARCH.FAILURE, error });
  }
}

function* watchMoviesLoad() {
  yield takeEvery(MOVIES.REQUEST, sagaMoviesLoad);
}

function* watchMoviesSearch() {
  yield takeEvery(SEARCH.REQUEST, sagaMoviesSearch);
}

export default function* root() {
  yield all([fork(watchMoviesLoad), fork(watchMoviesSearch)]);
}
