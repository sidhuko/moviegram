export const colors = {
  white: '#f9f9f9',
  black: '#0D0D0D',
  navy: '#0E1126',
  pink: '#BF0637',
  red: '#A60825',
  deepRed: '#73060F',
};

export const palette = {
  primary: colors.pink,
  secondary: colors.navy,
  white: colors.white,
  black: colors.black,
};
