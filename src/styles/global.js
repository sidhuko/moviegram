import { createGlobalStyle } from 'styled-components';
import { palette } from './theme';

export default createGlobalStyle`
  body {
    @import url('https://fonts.googleapis.com/css?family=Montserrat');
    margin: 0;
    padding: 0;
    background: ${palette.secondary};
    font-family: 'Notable', sans-serif;
    color: ${palette.white};
  }
`;
