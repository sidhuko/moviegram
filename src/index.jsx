import * as React from 'react';
import { render } from 'react-dom';
import { Router, Route, Switch, Redirect } from 'react-router-dom';

import sagas from './sagas';
import history from './services/history';
import Root from './containers/Root/Root';
import configureStore from './store/configureStore';
import Application from './components/Application/Application';
import GlobalStyle from './styles/global';

const store = configureStore();

store.runSaga(sagas);

render(
  <>
    <GlobalStyle />
    <Root store={store}>
      <Router history={history}>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/movies" />} />
          <Route path="/movies" component={Application} />
          <Route path="*" render={() => <h1>Page not found</h1>} />
        </Switch>
      </Router>
    </Root>
  </>,
  document.getElementById('root')
);
