const REQUEST = 'REQUEST';
const SUCCESS = 'SUCCESS';
const FAILURE = 'FAILURE';

function createRequestTypes(base) {
  return [REQUEST, SUCCESS, FAILURE].reduce((acc, type) => {
    acc[type] = `${base}_${type}`;
    return acc;
  }, {});
}

function action(type, payload = {}) {
  return { type, ...payload };
}

// Constants
export const MOVIES = createRequestTypes('MOVIES');
export const SEARCH = createRequestTypes('SEARCH');

// Dispatch helpers
export const loadMovies = () => action(MOVIES.REQUEST);
export const searchMovies = query => action(SEARCH.REQUEST, { query });
