import { loadMovies, searchMovies, MOVIES, SEARCH } from './index';

it('loadMovies dispatch', () => {
  expect(loadMovies()).toEqual({ type: MOVIES.REQUEST });
});

it('searchMovies dispatch', () => {
  const query = 'test';
  expect(searchMovies(query)).toEqual({ type: SEARCH.REQUEST, query });
});
