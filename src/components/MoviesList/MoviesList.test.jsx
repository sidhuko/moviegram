import React from 'react';
import { shallow } from 'enzyme';
import MoviesList, { noMoviesAvailable } from './MoviesList';
import MoviesListItem from '../MoviesListItem/MoviesListItem';

const title = 'Example Title';
const storyline = 'Example storyline';
const posterurl = 'an-image.jpg';

const movies = [
  {
    id: '1',
    title,
    storyline,
    posterurl,
  },
  {
    id: '2',
    title,
    storyline,
    posterurl,
  },
];

describe('MoviesList', () => {
  describe('with no movies', () => {
    const tree = shallow(<MoviesList />);

    it('should match snapshot for no movies', () => {
      expect(tree).toMatchSnapshot();
    });

    it('should render no movies available message', () => {
      expect(tree.contains(noMoviesAvailable)).toBeTruthy();
    });
  });

  describe('with movies', () => {
    const tree = shallow(<MoviesList movies={movies} />);

    it('should match snapshot with movies', () => {
      expect(tree).toMatchSnapshot();
    });

    it('should not render no movies available', () => {
      expect(tree.contains(noMoviesAvailable)).toBeFalsy();
    });

    it('should render movies', () => {
      const children = tree.find(MoviesListItem);
      expect(children).toHaveLength(2);
    });
  });

  describe('with search results', () => {
    const tree = shallow(
      <MoviesList query="test" search={movies} movies={movies} />
    );

    it('should match snapshot with results', () => {
      expect(tree).toMatchSnapshot();
    });

    it('should render movies', () => {
      const children = tree.find(MoviesListItem);
      expect(children).toHaveLength(2);
    });
  });

  describe('with search but no results', () => {
    const tree = shallow(
      <MoviesList query="test" search={[]} movies={movies} />
    );

    it('should match snapshot with no results', () => {
      expect(tree).toMatchSnapshot();
    });

    it('should render no movies', () => {
      const children = tree.find(MoviesListItem);
      expect(children).toHaveLength(0);
    });

    it('should show no results message', () => {
      expect(tree.contains('No search results found')).toBeTruthy();
    });
  });
});
