import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import debounce from 'lodash.debounce';
import Header from '../Header/Header';
import MoviesListItem from '../MoviesListItem/MoviesListItem';

const NoMovies = styled.main`
  padding: 1em;
`;

const NoMoviesAvailable = styled.p`
  font-size: 1.2em;
  margin-bottom: 2em;
`;

const SearchBox = styled.input`
  border-radius: 1em;
  outline: none;
  font-size: 1em;
  padding: 0.3em 0.5em;
  box-sizing: border-box;
  margin-top: 1em;

  @media (min-width: 600px) {
    margin-top: 0;
  }
`;

const SearchResultsHeader = styled.h2`
  padding: 0 1em;
`;

export const noMoviesAvailable = (
  <NoMovies>
    <NoMoviesAvailable>Sorry, no moviews on show!</NoMoviesAvailable>
  </NoMovies>
);

const MoviesList = ({ query, movies, search, onSearch }) => {
  let results;
  let searchRef;

  if (!movies || movies.length <= 0) {
    return noMoviesAvailable;
  }

  const searchFn = ref =>
    debounce(() => onSearch(ref.value), 300, {
      maxWait: 1000,
    });

  if (query && search && search.length === 0) {
    results = (
      <>
        <SearchResultsHeader>No search results found</SearchResultsHeader>
      </>
    );
  } else if (query) {
    results = (
      <>
        <SearchResultsHeader>Search results</SearchResultsHeader>
        {search.map(({ id, title, storyline, posterurl }) => (
          <MoviesListItem
            id={id}
            key={`movie-${id}`}
            title={title}
            lead={storyline}
            image={posterurl}
          />
        ))}
      </>
    );
  } else {
    results = (
      <>
        {movies.map((
          { id, title, storyline, posterurl } // eslint-disable-next-line sonarjs/no-identical-functions
        ) => (
          <MoviesListItem
            id={id}
            key={`movie-${id}`}
            title={title}
            lead={storyline}
            image={posterurl}
          />
        ))}
      </>
    );
  }

  return (
    <main>
      <Header>
        <SearchBox
          type="text"
          placeholder="Search"
          ref={el => {
            searchRef = el;
          }}
          onChange={() => searchFn(searchRef)()}
        />
      </Header>
      {results}
    </main>
  );
};

MoviesList.propTypes = {
  onSearch: PropTypes.func,
  query: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  search: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      storyline: PropTypes.string.isRequired,
      posterurl: PropTypes.string.isRequired,
    })
  ),
  movies: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      storyline: PropTypes.string.isRequired,
      posterurl: PropTypes.string.isRequired,
    })
  ),
};

MoviesList.defaultProps = {
  movies: [],
  search: [],
  query: false,
  onSearch: () => {},
};

export default MoviesList;
