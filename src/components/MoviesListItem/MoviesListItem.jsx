import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { transparentize } from 'polished';
import { palette } from '../../styles/theme';

const gradient = `linear-gradient(
  to bottom,
  ${transparentize(0.1, palette.secondary)},
  ${transparentize(0.3, palette.secondary)}
)`;

const Heading = styled.h2`
  font-size: 1.6em;
  margin: 0;
  padding: 0;
`;

const Movie = styled.article`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  padding: 2em;
  background: ${gradient};
  background: ${props =>
    `${gradient}, url(${props.background}) no-repeat center center`};
  background-size: cover;
  min-height: 10em;
  border-bottom: ${transparentize(0.8, palette.white)} 0.1em solid;

  @media (min-width: 600px) {
    flex-wrap: nowrap;
  }
`;

const Lead = styled.p`
  font-size: 1.2em;
  margin-bottom: 2em;
`;

const Button = styled(Link)`
  display: block;
  background: ${palette.primary};
  padding: 0.6em 1em;
  border-radius: 3px;
  user-select: none;
  text-align: center;
  text-transform: uppercase;
  text-decoration: none;
  color: ${palette.white};
  margin-bottom: 2em;
  position: relative;

  @media (min-width: 600px) {
    display: inline;
  }
`;

const Image = styled.img`
  width: 100%;
  flex-grow: 0;
  flex-shrink: 0;
  margin-right: 2em;
  margin-bottom: 2em;

  @media (min-width: 600px) {
    max-width: 10em;
    margin-bottom: 0;
  }
`;

const MovieDetails = styled.div`
  max-width: 60em;
`;

const Showtimes = styled.p`
  font-size: 1em;
  margin-bottom: 2em;
`;

const MoviesListItem = ({ id, title, lead, image }) => (
  <Movie background={image}>
    <Image src={image} alt={title} />
    <MovieDetails>
      <Heading>{title}</Heading>
      <Showtimes>
        <strong>Showtimes: </strong>
        12pm, 1pm
      </Showtimes>
      <Lead>{lead}</Lead>
      <Button to={`/movies/${id}`}>View information</Button>
    </MovieDetails>
  </Movie>
);

MoviesListItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  lead: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};

export default MoviesListItem;
