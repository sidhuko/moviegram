import React from 'react';
import { shallow } from 'enzyme';
import MoviesListItem from './MoviesListItem';

describe('MoviesListItem', () => {
  const id = '1';
  const title = 'Example title';
  const lead = 'Example lead';
  const image = 'image.jpg';
  const tree = shallow(
    <MoviesListItem id={id} image={image} lead={lead} title={title} />
  );

  it('should match snapshot', () => {
    expect(tree).toMatchSnapshot();
  });
});
