import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { transparentize } from 'polished';

import Header from '../Header/Header';
import history from '../../services/history';
import { palette } from '../../styles/theme';

const gradient = `linear-gradient(
  to bottom,
  ${transparentize(0.1, palette.secondary)},
  ${transparentize(0.3, palette.secondary)}
)`;

const Heading = styled.h2`
  font-size: 1.6em;
  margin: 0;
  padding: 0;
`;

const Movie = styled.article`
  padding: 2em;
  background: ${gradient};
  background: ${props =>
    `${gradient}, url(${props.background}) no-repeat center center`};
  background-size: cover;
  min-height: 10em;
  border-bottom: ${transparentize(0.8, palette.white)} 0.1em solid;
`;

const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin-bottom: 2em;

  @media (min-width: 600px) {
    flex-wrap: nowrap;
  }
`;

const Lead = styled.p`
  font-size: 1.2em;
  margin-bottom: 2em;
`;

const Button = styled.button`
  background: ${palette.primary};
  padding: 0.6em 1em;
  border-radius: 3px;
  font-size: 1.2em;
  user-select: none;
  text-align: center;
  text-transform: uppercase;
  text-decoration: none;
  color: ${palette.white};
  margin: 0 0 2em 0;
  position: relative;
  border: none;
`;

const Image = styled.img`
  width: 100%;
  flex-grow: 0;
  flex-shrink: 0;
  margin-right: 2em;
  margin-bottom: 2em;

  @media (min-width: 600px) {
    max-width: 12em;
    margin-bottom: 0;
  }
`;

const MovieDetails = styled.div`
  max-width: 60em;
`;

const Showtimes = styled.p`
  font-size: 1.2em;
  margin-bottom: 2em;
  min-width: 10em;
  max-width: 10em;
  margin-right: 2em;
`;

const MoviePage = ({ movie }) => {
  const { title, storyline, posterurl } = movie;
  return (
    <>
      <Header />
      <Movie background={posterurl}>
        <Button type="button" onClick={history.goBack}>
          Back
        </Button>
        <Row>
          <Image src={posterurl} alt={title} />
          <MovieDetails>
            <Heading>{title}</Heading>
          </MovieDetails>
        </Row>
        <Row>
          <Showtimes>
            <strong>Showtimes: </strong>
            12pm, 1pm
          </Showtimes>
          <Lead>{storyline}</Lead>
        </Row>
      </Movie>
    </>
  );
};

MoviePage.propTypes = {
  movie: PropTypes.shape({
    title: PropTypes.string.isRequired,
    storyline: PropTypes.string.isRequired,
    posterurl: PropTypes.string.isRequired,
  }),
};

MoviePage.defaultProps = {
  movie: false,
};

export default MoviePage;
