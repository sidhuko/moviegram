import React from 'react';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import Application from './Application';

const Child = () => <div>Hello World</div>;
const ManyChild = () => (
  <>
    <Child />
    <Child />
  </>
);

describe('Application', () => {
  describe('with single child element', () => {
    const application = mount(
      <MemoryRouter>
        <Application>
          <Child />
        </Application>
      </MemoryRouter>
    );

    it('should match snapshot', () => {
      expect(application.find(Application)).toMatchSnapshot();
    });
  });

  describe('with array child element', () => {
    const application = mount(
      <MemoryRouter>
        <Application>
          <ManyChild />
        </Application>
      </MemoryRouter>
    );

    it('should match snapshot', () => {
      expect(application.find(Application)).toMatchSnapshot();
    });
  });
});
