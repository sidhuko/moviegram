import React from 'react';

import { hot } from 'react-hot-loader/root';
import { Route } from 'react-router-dom';

import ConnectedMovie from '../../containers/Movie/Movie';
import ConnectedMovies from '../../containers/Movies/Movies';

const Application = () => {
  return (
    <>
      <Route exact path="/movies" component={ConnectedMovies} />
      <Route path="/movies/:id" component={ConnectedMovie} />
    </>
  );
};

export default hot(Application);
