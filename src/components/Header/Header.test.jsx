import React from 'react';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import Header from './Header';

describe('Header', () => {
  const tree = mount(
    <MemoryRouter>
      <Header />
    </MemoryRouter>
  );

  it('should match snapshot', () => {
    expect(tree.find(Header)).toMatchSnapshot();
  });

  it('should include logo', () => {
    expect(tree.contains('🎬')).toBeTruthy();
  });

  it('should include brandname', () => {
    expect(tree.contains('Moviegram')).toBeTruthy();
  });

  it('should include link to homepage', () => {
    expect(tree.find('Header__HomeLink').prop('to')).toEqual('/');
  });
});
