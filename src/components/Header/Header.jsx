import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { transparentize } from 'polished';
import { palette } from '../../styles/theme';

const Heading = styled.h1`
  font-size: 1.2em;
  font-weight: normal;
  text-transform: uppercase;
  padding: 0;
  margin: 0;
`;

const HomeLink = styled(Link)`
  color: ${transparentize(0.3, palette.white)};
  text-decoration: none;

  &:hover,
  &:active {
    color: ${transparentize(0, palette.white)};
  }
`;

const Wrapper = styled.header`
  padding: 1em;
  display: flex;
  justify-content: space-between;
  align-content: inherit;
  flex-direction: column;

  @media (min-width: 600px) {
    align-content: stretch;
    flex-wrap: nowrap;
    flex-direction: row;
  }
`;

const Header = ({ children }) => (
  <Wrapper>
    <Heading>
      <HomeLink to="/">
        <span role="img" aria-label="Movies">
          🎬
        </span>
        Moviegram
      </HomeLink>
    </Heading>
    {children}
  </Wrapper>
);

Header.propTypes = {
  children: PropTypes.element,
};

Header.defaultProps = {
  children: React.createElement('div'),
};

export default Header;
