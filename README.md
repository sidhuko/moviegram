## Prerequisites

- Node
- Yarn

Please refer to the [package.json](package.json#L10) engines property for compatiable versions.

## Scripts

The following scripts are available for contributing and building the project.

### `yarn start`

Starts the webpack developement server with hotreloading for development.

---

### `yarn test`

Runs jest test suite across codebase

---

### `yarn lint`

Runs ESLint and Stylelint against the codebase

##### `yarn lint:js`

Will only run the ESlint tooling

##### `yarn lint:css`

Will only run the Stylelint tooling

---

### `yarn build`

Builds the production assets to the `dist` directory.

---

## Hooks

When installed the project will use `husky` to automatically setup code quality checks against the git workflow.

### Pre-commit

Runs ESlint, Stylelint and Prettier against staged files.

### Commit-msg

Checks commit message against `@commitlint/config-conventional` conventions.

### Pre-push

Runs the test suite (`yarn test`).
